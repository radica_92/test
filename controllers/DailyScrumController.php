<?php

namespace app\controllers;

use Yii;
use app\models\DailyScrum;
use app\models\DailyScrumSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;

/**
 * DailyScrumController implements the CRUD actions for DailyScrum model.
 */
class DailyScrumController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
			  'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'delete','index','view'],
                'rules' => [
                   
					 [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete','index','view'],
                        'roles' => ['admin'],
                    ],
                ],
			],
        ];
    }

    /**
     * Lists all DailyScrum models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DailyScrumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DailyScrum model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DailyScrum model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
			$model = new DailyScrum();

			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				return $this->redirect(['view', 'id' => $model->id]);
			}

        return $this->render('create', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing DailyScrum model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
			
			 $model = $this->findModel($id);

			if ($model->load(Yii::$app->request->post()) && $model->save()) {
				return $this->redirect(['view', 'id' => $model->id]);
			}

			return $this->render('update', [
				'model' => $model,
			]);
		
    }

    /**
     * Deletes an existing DailyScrum model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
       
    }

    /**
     * Finds the DailyScrum model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DailyScrum the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DailyScrum::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
