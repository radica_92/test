<?php
use yii\helpers\Html;
use dosamigos\chartjs\ChartJs;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */

$this->title = 'Employee records';

?>
<div class="site-index">

    <div class="jumbotron">
        <h1></h1>

        <p >>> Application that makes it easy to track and edit employee records <<</p>

		
		<?= Html::img('@web/uploads/logo.png', ['alt'=>'Walter']);?> 	

    </div>

    <div class="body-content">

<!-- Ovo rucno unosenje u data i labele nije bas prakticno,ali je grafik cisto izgleda radi -->
        <div class="row" id="charts">
            <div class="col-xs-12 chart text-center" >
				<?= ChartJs::widget([
					'type' => 'bar',
					'id' => 'chart1',
					'options' => [
						'height' => 300,
						'width' => 400
					],
					'data' => [
						'labels' => [$employees_array[0], $employees_array[1], $employees_array[2],$employees_array[3],$employees_array[4],$employees_array[5],$employees_array[6],$employees_array[7]],
						 'datasets' => [
							[
								'label' => "Employees",
								'backgroundColor' => "rgba(0, 143, 196, 1)",
								'borderColor' => "rgba(179,181,198,1)",
								'pointBackgroundColor' => "rgba(179,181,198,1)",
								'pointBorderColor' => "#fff",
								'pointHoverBackgroundColor' => "#fff",
								'pointHoverBorderColor' => "rgba(179,181,198,1)",
								'data' => [$employees_array[0], $employees_array[1], $employees_array[2],$employees_array[3],$employees_array[4],$employees_array[5],$employees_array[6],$employees_array[7]]
							],
							
						],
							 'datasets' => [
							[
								'label' => "Been late total ",
								'backgroundColor' => "rgba(0, 143, 196, 1)",
								'borderColor' => "rgba(179,181,198,1)",
								'pointBackgroundColor' => "rgba(179,181,198,1)",
								'pointBorderColor' => "#fff",
								'pointHoverBackgroundColor' => "#fff",
								'pointHoverBorderColor' => "rgba(179,181,198,1)",
								'data' => [$been_late_array[0],$been_late_array[1],$been_late_array[2],$been_late_array[3],$been_late_array[4],$been_late_array[5],$been_late_array[6],$been_late_array[7]]
							]
							
						]
					]
				]);
				?>
				
				<?= ChartJs::widget([
					'type' => 'horizontalBar',
					'id' => 'chart2',
					'options' => [
						'height' => 300,
						'width' => 400
					],
					'data' => [
						'labels' => [$employees_array[0], $employees_array[1], $employees_array[2],$employees_array[3],$employees_array[4],$employees_array[5],$employees_array[6],$employees_array[7]],
						 'datasets' => [
							[
								'label' => "Employees",
								'backgroundColor' => "rgba(0, 143, 196, 1)",
								'borderColor' => "rgba(179,181,198,1)",
								'pointBackgroundColor' => "rgba(179,181,198,1)",
								'pointBorderColor' => "#fff",
								'pointHoverBackgroundColor' => "#fff",
								'pointHoverBorderColor' => "rgba(179,181,198,1)",
								'data' => [$employees_array[0], $employees_array[1], $employees_array[2],$employees_array[3],$employees_array[4],$employees_array[5],$employees_array[6],$employees_array[7]]
							],
							
						],
							 'datasets' => [
							[
								'label' => "Been late total by individual employees",
								'backgroundColor' => "rgba(0, 143, 196, 1)",
								'borderColor' => "rgba(179,181,198,1)",
								'pointBackgroundColor' => "rgba(179,181,198,1)",
								'pointBorderColor' => "#fff",
								'pointHoverBackgroundColor' => "#fff",
								'pointHoverBorderColor' => "rgba(179,181,198,1)",
								'data' => [$been_late_array[0],$been_late_array[1],$been_late_array[2],$been_late_array[3],$been_late_array[4],$been_late_array[5],$been_late_array[6],$been_late_array[7]]
							]
							
						]
					]
				]);
				?>

            </div>

        </div>

    </div>
</div>
