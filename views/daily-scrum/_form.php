<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ActiveField;

/* @var $this yii\web\View */
/* @var $model app\models\DailyScrum */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="daily-scrum-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'meeting_start')->textInput()->label('Start time')?>

    <?= $form->field($model, 'date')->widget(\yii\jui\DatePicker::class, [
    'dateFormat' => 'yyyy-MM-dd', 
	])->label('Date of a meeting')
	?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
