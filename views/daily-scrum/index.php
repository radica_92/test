<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View
@var $searchModel app\models\DailyScrumSearch
@var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Daily Scrums');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daily-scrum-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Daily Scrum'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
		//uklanjanje kolone sa rednim brojem (#)
          //  ['class' => 'yii\grid\SerialColumn'],

            'id',
            'meeting_start',
            'date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	</div>
</div>
