<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Participation */

$this->title = Yii::t('app', 'Update Participation: ' . $model->daily_scrum_id, [
    'nameAttribute' => '' . $model->daily_scrum_id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Participations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->daily_scrum_id, 'url' => ['view', 'daily_scrum_id' => $model->daily_scrum_id, 'user_id' => $model->user_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="participation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'start' => $start,
		'employee_names' => $employee_names,
    ]) ?>

</div>
