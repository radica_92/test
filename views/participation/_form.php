<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Participation */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="participation-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'daily_scrum_id')->textInput()->label('Daily scrum date')->dropdownList(
        $start
    ,
    ['prompt'=>'Select Date...']
)->label('Daily scrum date');?>

    <?= $form->field($model, 'user_id')->dropdownList(
        $employee_names
    ,
    ['prompt'=>'Select Employee...']
)->label('Employee'); ?>

    <?= $form->field($model, 'time_of_arrival')->hint('hh:mm:ss format')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
