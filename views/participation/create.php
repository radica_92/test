<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Participation */

$this->title = Yii::t('app', 'Create Participation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Participations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
		'employee_names' => $employee_names,
		'start' => $start,
    ]) ?>

</div>
