<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ParticipationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Participations');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Participation'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],

            //'daily_scrum_id',
              //'user_id',
           'time_of_arrival',
           [ 
               'label'=> 'Employee', 
               'attribute' => 'user_id', 
			   // umjesto id-a ispisuje ime i prezime radnika u jednoj koloni
               'value' => function($model){ 
                   $employees=app\models\User::find()->where(['id'=>$model->user_id])->one(); 
                   return $employees->first_name . ' ' .$employees->last_name; 
               } 
           ], 
		   [ 
               'label'=> 'Date', 
               'attribute' => 'daily_scrum_id', 
			   // umjesto id-a ispisuje ime i prezime radnika u jednoj koloni
               'value' => function($model){ 
                   $date=app\models\DailyScrum::find()->where(['id'=>$model->daily_scrum_id])->one(); 
                   return $date->date; 
               } 
           ], 
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	</div>
</div>
