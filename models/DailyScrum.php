<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "daily_scrum".
 *
 * @property int $id
 * @property string $meeting_start
 * @property string $date
 *
 * @property Participation[] $participations
 * @property User[] $users
 */
class DailyScrum extends \yii\db\ActiveRecord
{
	    public function init()
    { 
		$this->meeting_start='08:45:00';
		//$this->date = $this->find()->orderBy(['id' => SORT_DESC])->one();
        parent::init();
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'daily_scrum';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['meeting_start', 'date'], 'safe'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meeting_start' => 'Meeting start time',
            'date' => 'Date of meeting',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipations()
    {
        return $this->hasMany(Participation::className(), ['daily_scrum_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('participation', ['daily_scrum_id' => 'id']);
    }
}
