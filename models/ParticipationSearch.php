<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Participation;

/**
 * ParticipationSearch represents the model behind the search form of `app\models\Participation`.
 */
class ParticipationSearch extends Participation
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
             [['daily_scrum_id'], 'integer'],
             [['daily_scrum_id'], 'integer'],
             [['time_of_arrival'], 'safe'],
             [['user_id'], 'string'], 
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Participation::find();
		 //kako bi radio search u tabeli,jer umjesto user_id prikazujemo ime zaposlenog 
        $query -> leftJoin('user','user.id = participation.user_id'); 
		
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'daily_scrum_id' => $this->daily_scrum_id,
            'time_of_arrival' => $this->time_of_arrival,
        ]);
		
		 $query->andFilterWhere(['like','user.first_name',$this->user_id]);

        return $dataProvider;
    }
}
